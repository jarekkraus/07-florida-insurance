package uj.java.pwj2019.w7;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

public class FloridaInsurance {

    private static List<InsuranceEntry> createInsuranceEntryFromCSV(String fileName) throws IOException {
        var zipFile = new ZipFile(fileName);
        var output = new ArrayList<InsuranceEntry>();
        var entries = zipFile.entries();
        var entry = entries.nextElement();
        InputStream inputStream = zipFile.getInputStream(entry);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = bufferedReader.readLine();
        while ((line = bufferedReader.readLine()) != null) {
            var tmp = line.split(",");
            output.add(new InsuranceEntry(
                    Integer.parseInt(tmp[0]),
                    tmp[2],
                    new BigDecimal(tmp[7]),
                    new BigDecimal(tmp[8]),
                    Line.valueOf(tmp[15].toUpperCase()),
                    tmp[16],
                    Double.parseDouble(tmp[13]),
                    Double.parseDouble(tmp[14])
            ));
        }
        bufferedReader.close();
        inputStream.close();
        zipFile.close();
        return output;
    }

    private static File createFile(String path) throws IOException {
        File file = new File(path);
        if(!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    public static void main(String[] args) {
        try {
            List<InsuranceEntry> data = createInsuranceEntryFromCSV("FL_insurance.csv.zip");
            File countFile = createFile("count.txt");
            File tiv2012File = createFile("tiv2012.txt");
            File most_valuableFile = createFile("most_valuable.txt");

            try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(countFile.getPath())))) {
                pw.print(
                        data
                                .stream()
                                .collect(Collectors.groupingBy(InsuranceEntry::country))
                                .size()
                );
            }

            try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(tiv2012File.getPath())))) {
                pw.print(
                        data
                                .stream()
                                .collect(Collectors.reducing(BigDecimal.ZERO, InsuranceEntry::tiv2012, BigDecimal::add))
                );
            }

            try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(most_valuableFile.getPath())))) {
                pw.println("country,value");
                data
                        .stream()
                        .collect(Collectors.groupingBy(InsuranceEntry::country,
                                Collectors.mapping(el -> el.tiv2012().subtract(el.tiv2011()),
                                        Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))))
                        .entrySet()
                        .stream()
                        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                        .limit(10)
                        .map(line -> line.toString().replace("=", ","))
                        .forEach(pw::println);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
